Examples
========


List all ssl certificates
-------------------------

-------------------------

tmsh

cd /

list sys crypto recursive cert

SSL decrypt
-----------

-----------

**Capture the traffic**
  ``tcpdump -vvv -s0 -nni0.0 -c 100 -w /tmp/https.cap``


**Create an irule**

.. code-block:: bash

  when CLIENTSSL_HANDSHAKE {
    log local0.info "RSA Session-ID:[SSL::sessionid] Master-Key:[SSL::sessionsecret]"
  }

  when SERVERSSL_HANDSHAKE {
    log local0.info "RSA Session-ID:[SSL::sessionid] Master-Key:[SSL::sessionsecret]"
  }


**Generate a self-signed certifcate**

  ``openssl req -new -x509 -keyout ssl_cert_path -out ssl_cert_path -days 365 -nodes -subj /CN=1ce8erg0.local/O=1ce8erg0/C=AU``


**Running a SSL server in Python**

.. code-block:: python

  import BaseHTTPServer
  import SimpleHTTPServer
  import ssl
  import logging

  httpd = BaseHTTPServer.HTTPServer(("0.0.0.0", 443), SimpleHTTPServer.SimpleHTTPRequestHandl
  er)
  httpd.socket = ssl.wrap_socket(httpd.socket, certfile="./ssl_cert", server_side=True)
  httpd.serve_forever()


Export the PMS file
  ``grep Session-ID /var/log/ltm | sed 's/.*\(RSA.*\)/\1/' > session.pms``


Open Wireshark. Preferences, Protocol, SSL ->


Ansible
-------

An example to configure the F5 in Ansible.

.. code-block:: python

  ---
  - name: Create a VIP, pool, pool members and nodes
    hosts: 192.168.14.10
    connection: local
    strategy: debug
    gather_facts: no


    tasks:
      - name: run show version on remote device
        bigip_command:
          commands: show sys version
          server: "192.168.14.10"
          password: "admin"
          user: "admin"
          server_port: "443"
          validate_certs: "no"
        delegate_to: localhost
        register:  f5_result

      - name: run multiple commands on remote nodes
        bigip_command:
          commands:
            - show sys version
            - list ltm virtual
          server: "192.168.14.10"
          password: "admin"
          user: "admin"
          validate_certs: "no"
        delegate_to: localhost

      - name: run multiple commands and evaluate the output
        bigip_command:
          commands:
            - show sys version
            - list ltm virtual
          wait_for:
            - result[0] contains BIG-IP
            - result[1] contains Pyspider
          server: "192.168.14.10"
          password: "admin"
          user: "admin"
          validate_certs: "no"
        delegate_to: localhost

      - name: save the running configuration of BIG-IP
        bigip_config:
          save: yes
          server: "192.168.14.10"
          password: "admin"
          user: "admin"
          validate_certs: "no"
        delegate_to: localhost


      - name: Create a pool
        bigip_pool:
          name: "ANSIBLE-WEB"
          password: "admin"
          server: "192.168.14.10"
          slow_ramp_time: "120"
          user: "admin"
          validate_certs: "no"
        delegate_to: localhost

      - name:  Backup ucs file
        bigip_ucs:
          password: "admin"
          server: "192.168.14.10"
          dest: "/tmp/f5_backup.ucs"
          src: "backup.ucs"
          user: admin
        delegate_to: localhost



F5-SDK
------

.. code-block:: python

  from f5.bigip import ManagementRoot
  mgmt = ManagementRoot("192.168.14.10", "admin", "admin")

  pools = mgmt.tm.ltm.pools.get_collection()
  for pool in pools:
      print pool.name
      for member in pool.members_s.get_collection():
          print member.name

  try:
      from f5.bigip import ManagementRoot
      from f5.bigip.contexts import TransactionContextManager
      HAS_F5SDK = True
  except ImportError:
      HAS_F5SDK = False


  try:
      result = api.tm.ltm.pools.pool.create(foo='bar')
  except F5ModuleError as e:
      module.fail_json(msg=e.message)

  def main():

     if not HAS_F5SDK:
        module.fail_json(msg='f5-sdk required for this module')

  from f5.bigip import ManagementRoot

  mgmt = ManagementRoot("bigip.example.com", "admin", "somepassword")

  pools = mgmt.tm.ltm.pools.get_collection()
  for pool in pools:
      print pool.name
      for member in pool.members_s.get_collection():
          print member.name

  mypool = mgmt.tm.ltm.pools.pool.create(name='mypool', partition='Common')

  pool_a = mgmt.tm.ltm.pools.pool.load(name='mypool', partition='Common')
  pool_a.description = "New description"
  pool_a.update()

  if mgmt.tm.ltm.pools.pool.exists(name='mypool', partition='Common'):
      pool_b = mgmt.tm.ltm.pools.pool.load(name='mypool', partition='Common')
      pool_b.delete()



LTM External Monitor
--------------------

The rule is very simple.

  **IF ANY VALUE AT ALL GOES TO STANDARD OUTPUT, THE POOL MEMBER WILL BE MARKED UP.**


curl-monitor
+++++++++++++

.. code-block:: python

  #!/bin/bash

  MONITOR_NAME="curl-monitor"
  pidfile="/var/run/$MONITOR_NAME.pid"

  if [ -f $pidfile ]
  then
     kill -9 `cat $pidfile` > /dev/null 2>&1
  fi

  echo "$$" > $pidfile


  /usr/bin/curl -Ik https://10.10.10.2/test   | grep -E -i ok > /var/tmp/foo.txt

  status=$?
  if [ $status -eq 0 ]
  then
      rm -f $pidfile
      echo "UP"
      exit 0
  fi


  # Remove the pidfile before the script ends
  rm -f $pidfile

  exit 1
