README.md
# F5-CheatSheet
===============


Status
------
  *Updating....*

Run the server
--------------
.. code-block:: console

    $ virtualenv  ./env
    $ source ./env/bin/activate
    $ pip install -r requirements.txt
    $ make
    $ python app.py

    # URL: localhost:5000

Demo
====

- `heroku`_


.. _heroku: https://f5-cheatsheet.herokuapp.com/
