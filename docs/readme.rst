README
======

How to run the server
---------------------
.. code-block:: console

    $ virtualenv  ./env
    $ source ./env/bin/activate
    $ pip install -r requirements.txt
    $ make
    $ python app.py

    # URL: localhost:5000


.. Master file, created by
   sphinx-quickstart on Wed Aug 23 22:48:02 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
