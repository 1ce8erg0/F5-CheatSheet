
=====
Basic


Modules
--------------

--------------

* BIG-IP Access Policy Manager **(APM)** - Authentication and ssl vpn

* BIG-IP Local Traffic Manager **(LTM)** - *Load Balancing Traffic*
* BIG-IP Application Security Manager **(ASM)** - *Web application firewall*
* BIG-IP Global Traffic Manager **(GTM)** - *DNSSEC*
* BIG-IP Application Acceleration Manager **(AAM)** - *WAN Acceleration*
* BIG-IP Advanced Firewall Manager **(AFM)** - *Layer 4 firewall module*

Downlaod the VM
---------------

You will need to `register an account to be able to request 3 trials keys for 90 days free. <https://f5.com/register?returnurl=%2Fproducts%2Ftrials%2Fproduct-trials>`_



`Download <https://f5.com/products/trials/product-trials>`_ **a Free 90 Days Trials**

---------------

Here is what's included in this full-featured product trial:

* Advanced traffic management, SSL, and load balancing (LTM)
* DNS and global server load balancing (DNS)
* Federated application and network access (APM)
* Data center firewall and DDoS protection (AFM)
* Web application firewall (ASM)
* Dataplane programmability (iRules)
* Full-featured management API (iControl)
* Centralized management platform for BIG-IP (BIG-IQ)

---------------

If you don't have VMWare WorkStation. You can convert **qcow2** to **vdi** (Virtualbox),

``qemu-img convert -O vdi ./BIGIP-11.6.0.0.0.401.qcow2 BIGIP-11.6.0.0.401.vdi``

--------------


Login
-----

--------------

Default::

  ssh: root / default
  web: admin / admin

To create another local user::

  create /auth user <admin-user> partition-access add { all-partitions { role admin } } prompt-for-password

Modify the admin user

  modify /sys db systemauth.primaryadminuser value <user>

  create /auth user <username> shell bash partition-access add { all-partitions { role admin } } prompt-for-password

  create /auth user newrootuser shell bash partition-access add { all-partitions { role admin } } prompt-for-password

  modify /sys db systemauth.disablerootlogin value true

--------------


Activate License
----------------

--------------

First, get a key - `Generate key <https://www.f5.com/trial/secure/generate-eval-key.php>`_

  Run   ``get-dossier -b  License Key``


Go to Web and *Paste* into `Dossier.jsp <https://activate.f5.com/license/dossier.jsp>`_ then copy to:

  Paste the license into   ``vi /config/bigip.license``

*Reload* the license

  **reloadlic**

-------------------

Configure interface
-------------------

-------------------

Displaying Management interface


  ``tmsh /sys mangement-ip``

  ``tmsh /sys management-route``


Creating Management Interface

  ``create /sys management-ip [ip address/prefixlen]``

  ``create /sys management-route default gateway <gateway ip address>``




iRules
------

* iRules Editor Download: https://devcentral.f5.com/d/irule-editor
* Sublime Syntax Highlight: https://github.com/billchurch/sublime-iRules



Debug
-----

-----

Show Persistent records
+++++++++++++++++++++++

  modify /sys db log.ssl.level value Warning

  tmsh run sys crypto check-cert - tee -a  /tmp/cert.txt


  tmsh show ltm persistence persist-records

  tmsh show sys conn cs-client-addr 100.1.1.1 cs-server-addr 10.1.1.0 ss-server-addr 192.168.1.1 ss-server-port 9999 all-properties


Monitor
+++++++

+++++++

* tmsh show /sys hardware
* system_state
* ``listconfig``
* ``bigtop``
* ``/etc/init.d/syslog-ng restart``
* tail -f \*.log ltm

Backup and Reload
+++++++++++++++++

+++++++++++++++++

* tmsh save /sys config partitions all
* save /sys ucs <path/to/UCS> passphrase <password>
* load /sys ucs <path/to/UCS>


Display Pending Changes
+++++++++++++++++++++++

+++++++++++++++++++++++

* tmsh show cm device-group all incremental-config-sync-cache
