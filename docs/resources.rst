URL Resources
-------------

Study guide
+++++++++++

`Application Delivery Fundamental - 101 <https://www.f5.com/pdf/certification/exams/Certification_Study_Guide_101.pdf>`_

`TMOS 201 <https://f5.learn.com/files/upload/CertExamStudyGuides/F5 201 - Study Guide - TMOS Administration r2.pdf>`_


F5 Offical sites
++++++++++++++++

* `Troubleshooting Wizard <https://support.f5.com/csp/guided-search>`_
* `DevCentral <https://devcentral.f5.com/>`_
* `Ansbile <https://f5-ansible.readthedocs.io/en/devel/modules/list_of_all_modules.html>`_
* `News <https://support.f5.com/csp/new-updated-articles>`_
* `University <https://university.f5.com>`_
* `Strong_Box <http://strongbox.f5.com>`_
* `License_Activation <https://activate.f5.com/license/dossier.jsp>`_
* `Trials_download <https://f5.com/products/trials/product-trials>`_
* `Service_Request <https://support.f5.com/csp/my-support/service-request>`_
* `Exam_guide <https://f5.com/education/certification/certification-exams>`_
* `Exam_Registration <https://www.certmetrics.com/f5certified/login.aspx>`_
* `Sliverline <https://f5.com/products/deployment-methods/silverline/cloud-based-ddos-protection>`_


Others Related
++++++++++++++

https://devcentral.f5.com/questions/ssldump-and-internal-hsm

https://support.f5.com/csp/article/K15292

https://firstdigest.com/2014/12/how-to-integrate-f5-big-ip-ve-with-gns3/


