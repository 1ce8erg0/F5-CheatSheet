import os
# from flask_sslify import SSLify
from flask import Flask, abort, send_from_directory

from flask_sslify import SSLify

DIR = os.path.dirname(__file__)
ROOT = os.path.join(DIR, 'docs', '_build_html', 'html')


app = Flask(__name__)

if 'DYNO' in os.environ: # only trigger SSLify if the app is running on Heroku
        sslify = SSLify(app)


@app.route('/')
def index_redirection():
  """Redirecting index file"""
  return send_from_directory(ROOT, "index.html")

@app.route('/<path:path>')
def static_path(path):
  """static file proxy"""
  return send_from_directory(ROOT, path)

@app.errorhandler(404)
def page_not_found(e):
  return send_from_directory(ROOT, "index.html")


if __name__=='__main__':
    os_port = int(os.environ.get('PORT', 33507))
    app.run(debug=True, host="0.0.0.0", port=os_port)
